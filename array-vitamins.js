const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

function itemsAvailable(items) {
  return items.filter((items) => {
    return items.available;
  });
}

function containsOnlyVitamin(items, vitamin) {
  return items.filter((items) => {
    if (items.contains === vitamin) {
      return true;
    }

    return false;
  });
}

function containsVitamin(items, vitamin) {
  return items.filter((items) => {
    return items.contains.includes(vitamin);
  });
}

function groupItemsBasedOnVitamins(items) {
  return items.reduce((accu, item) => {
    const vitaminArray = item.contains.split(",");

    vitaminArray.forEach((vitamin) => {
      const vitaminTrim = vitamin.trim();

      if (accu[vitaminTrim] !== undefined) {
        accu[vitaminTrim].push(item.name);
      } else {
        accu[vitaminTrim] = [item.name];
      }
    });

    return accu;
  }, {});
}

function sortItems(items) {
  return items.sort((itemA, itemB) => {
    let item1Vitamins = itemA.contains.split(",");
    let item2Vitamins = itemB.contains.split(",");
    return item2Vitamins.length - item1Vitamins.length;
  });
}
